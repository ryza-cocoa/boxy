# boxy

Question box with Rust

My question box [ask.ryza.moe](https://ask.ryza.moe)

You may refer to my [blog post](https://ryza.moe/2019/12/rust-learning-from-zero-17/) for more detials~

![screenshot](screenshot.png)

## Usage
### With docker-compose
With docker-compose, it should be super easy.
```bash
cp conf/config.example.json conf/boxy.json
cp -rf web.exmaple web
docker-compose up -d
```

Port 5534 is exposed, and you can use nginx or Apache as proxy. And I opt for nginx, so code below is my configuration

```nginx
server {
  if ($host = ask.ryza.moe) {
    return 301 https://$host$request_uri;
  } # managed by Certbot

  listen 80;
  listen [::]:80;
  server_name ask.ryza.moe;
  return 404; # managed by Certbot
}

server {
  listen 443 ssl http2; # managed by Certbot
  listen [::]:443 ssl http2; # managed by Certbot
  
  server_name ask.ryza.moe; # managed by Certbot

  ssl_certificate /etc/letsencrypt/live/ryza.moe/fullchain.pem; # managed by Certbot
  ssl_certificate_key /etc/letsencrypt/live/ryza.moe/privkey.pem; # managed by Certbot
  include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
  ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
  
  ssl_session_cache shared:TLS:2m;
  ssl_buffer_size 4k;

  add_header Strict-Transport-Security "max-age=31536000";

  location / {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;

    proxy_pass http://localhost:5534;
    proxy_redirect off;
  }
}
```

### With my own MongoDB
And it's not hard if you prefer to use your own MongoDB server

First copy the example configuration file to `conf/boxy.json`

```bash
cp conf/config.example.json conf/boxy.json
cp -rf web.exmaple web
```

Then edit `conf/boxy.json` with your favorite editor. You need to specify `mongodb_url`, `mongodb_db`, `mongodb_username` and `mongodb_password`

```bash
docker build -t boxy .
docker run -d \
  --restart=always \
  -p 5534:5534 \
  -v "`pwd`/conf:/etc/boxy" \
  -v "`pwd`/web:/web" \
  boxy
```

## Customize Webpage

If you want to customize the webpage, then you may edit your web page in some directory, e.g, `/var/www/boxy`.

And now, if you are using Docker, then just map `/var/www/boxy/` to `/web`

If you are going to run this as a standalone application, you need to set `web_root` in `boxy.json` to `/var/www/boxy/`.

## API

You may refer to my [blog post](https://ryza.moe/2019/12/rust-learning-from-zero-17/) for API details. I'm currently too sleepy to copy those here.

## Acknowledgements
This project uses many other projects on GitHub or on the internet, without them this project can be done, but it will take literally forever XD. Massive thanks to them.

### Web
- [jQuery](https://jquery.com)
- [font-awesome](https://fontawesome.com)
- [rauldronca/pen/mEXomp](https://codepen.io/rauldronca/pen/mEXomp)
- [Tbgse/pen/JXrJGX](https://codepen.io/Tbgse/pen/JXrJGX)

### Rust
There are some crates that directly used in this project, but actucally there are plenty more crates behind these.

```
bson = "0.14"
crossbeam-channel = "0"
ctrlc = "3"
futures = "0.1"
hmac = "0.7"
hyper = "0.12"
lazy_static = "1.4"
mongodb = "0.9"
regex = "1"
serde_json = "1.0"
serde = "1"
sha2 = "0.8"
```
