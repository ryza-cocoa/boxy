#[macro_use(bson, doc)]
extern crate bson;
extern crate mongodb;
#[macro_use]
extern crate lazy_static;

mod boxy;

use boxy::{boxy_service, BoxyConfig, generate_hmac};
use futures::{future, Future};
use hyper::{
    Body,
    Request,
    Server,
    server::conn::AddrStream,
    service::{
        make_service_fn,
        service_fn
    }
};
use mongodb::{Client, Database, options::ClientOptions, options::auth::Credential};
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

lazy_static! {
    static ref CONFIG: BoxyConfig = from_cli_args();
    static ref PASSWORD_SALT: String = String::from("boxy$a1t");
}

fn main() -> Result<(), mongodb::error::Error> {
    // try to parse binding ip and port
    let (bind, port) = get_server_listen_options();
    let addr = format!("{}:{}", bind, port).parse().unwrap();
    
    hyper::rt::run(future::lazy(move || {
        // get mongodb client
        let client_options = get_mongodb_client_options().expect("[ERROR] Cannot get MongoDB client options");
        let client = Client::with_options(client_options).expect("[ERROR] Cannot connect to MongoDB");
        let database = match &CONFIG.mongodb_db {
            Some(db) => String::from(db),
            _ => String::from("boxy"),
        };
        let boxy_db = client.database(&database);
        let boxy_db_ref = boxy_db.clone();
        
        // set boxy account if not exists
        set_boxy_account(&boxy_db);
        
        // get web root directory
        let web_root = match &CONFIG.web_root {
            Some(web_root) => String::from(web_root),
            _ => String::from("/web"),
        };
        
        // make boxy service
        let boxy_service_fn = make_service_fn(move |socket: &AddrStream| {
            let remote_addr = socket.remote_addr();
            let boxy_db_ref = boxy_db_ref.clone();
            let web_root_ref = web_root.clone();
            service_fn(move |req: Request<Body>| {
                // create boxy service
                boxy_service(req, remote_addr, web_root_ref.clone(), &boxy_db_ref)
            })
        });
        
        // bind IP address and serve boxy service!
        let server = Server::bind(&addr)
            .serve(boxy_service_fn)
            .map_err(|e| eprintln!("[ERROR] server error: {}", e));
        println!("[INFO] Boxy on http://{}", addr);
        server
    }));

    Ok(())
}

fn set_boxy_account(db: &Database) {
    let username = &CONFIG.boxy_username;
    let password = &CONFIG.boxy_password;
    let password = generate_hmac(&*format!("{}{}", password, &*PASSWORD_SALT), &PASSWORD_SALT);
    
    let user_collection = db.collection("user");
    let number_users: i64 = user_collection.estimated_document_count(None).unwrap();
    if number_users == 0 {
        match user_collection.insert_one(doc!{
            "user":username,
            "password":password
        }, None) {
            Ok(_) => (),
            Err(e) => panic!("[ERROR] Cannot create user: {}", e),
        }
    }
}

fn from_cli_args() -> BoxyConfig {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        panic!("[ERROR] no config file provided.");
    }
    
    match load_config(&*args[1]) {
        Ok(value) => value,
        Err(e) => panic!("[ERROR] Cannot read config file {}", e),
    }
}

fn load_config<P: AsRef<Path>>(conf_path: P) -> Result<BoxyConfig, Box<dyn std::error::Error>> {
    // https://docs.serde.rs/serde_json/fn.from_reader.html#example

    // Open the file in read-only mode with buffer.
    let file = File::open(conf_path)?;
    let reader = BufReader::new(file);

    // Read the JSON contents of the file as an instance of `BoxyConfig`.
    let u = serde_json::from_reader(reader)?;

    // Return the `BoxyConfig`
    Ok(u)
}

fn get_server_listen_options() -> (String, u16) {
    let bind: String = match &CONFIG.bind {
        Some(bind) => match bind.len() {
            0 => String::from("127.0.0.1"),
            _ => String::from(bind),
        },
        _ => String::from("127.0.0.1"),
    };
    let port: u16 = match &CONFIG.port {
        Some(port) => *port,
        _ => 5534,
    };
    (bind, port)
}

fn get_mongodb_client_options() -> Result<ClientOptions, mongodb::error::Error> {
    let mut client_options = ClientOptions::parse(&CONFIG.mongodb_url)?;
    
    let username: Option<String> = match &CONFIG.mongodb_username {
        Some(username) => match username.len() {
            0 => None,
            _ => Some(String::from(username)),
        },
        _ => None,
    };
    let password: Option<String> = match &CONFIG.mongodb_password {
        Some(password) => match password.len() {
            0 => None,
            _ => Some(String::from(password)),
        },
        _ => None,
    };
    
    match (&username, &password) {
        (None, None) => (),
        (_, None) | (None, _) => panic!("[ERROR] Please provide both username and password."),
        (_, _) => {
            let credential_builder = Credential::builder()
                .username(username)
                .password(password);
            client_options.credential = Some(credential_builder.build());
        }
    };
    Ok(client_options)
}
