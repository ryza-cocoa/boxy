use futures::{future, Future, Stream};
use hyper::{Body, Method, Request, Response, StatusCode, header::CONTENT_TYPE};
use mongodb::Database;
use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::net::{IpAddr, SocketAddr};
use std::path::PathBuf;
use std::sync::{Arc, Mutex, atomic::AtomicU64, atomic::Ordering};

use super::boxy_answer::*;
use super::boxy_helper::*;
use super::boxy_token::*;
use super::boxy_question::*;

lazy_static! {
    static ref BOXY_API_V1_QUESTION_URI: Regex = Regex::new(r"^/api/v1/(question)|(answer/(\d+))$").unwrap();
    static ref BOXY_API_V1_DELETE_URI: Regex = Regex::new(r"^/api/v1/(question|answer)/(\d+)$").unwrap();
    static ref BOXY_API_V1_GET_URI: Regex = Regex::new(r"^/api/v1/question/(\d+)$").unwrap();
    static ref BOXY_API_V1_GET_FROM_URI: Regex = Regex::new(r"^/api/v1/question/from/(\d+)$").unwrap();
    static ref ACCESS_RATE: Arc<Mutex<HashMap<IpAddr, u64>>> = Arc::new(Mutex::new(HashMap::new()));
    static ref LAST_RESET_TIME: AtomicU64 = AtomicU64::new(get_timestamp());
}

// alias for generice error of hyper
type GenericError = Box<dyn std::error::Error + Send + Sync>;
// alias for response type of hyper
type ResponseFuture = Box<dyn Future<Item=Response<Body>, Error=GenericError> + Send>;

/// boxy service
pub fn boxy_service(req: Request<Body>, remote_addr: SocketAddr, web_root: String, db: &Database) -> ResponseFuture {
    let path = String::from(req.uri().path());
    let response = match req.method() {
        &Method::GET => api_get_response(&req, path, web_root, db),
        &Method::POST => return api_post_response(req, path, remote_addr, db),
        &Method::DELETE => return api_delete_response(req, path, db),
        // other HTTP methods
        _ => response_with_error(405, StatusCode::METHOD_NOT_ALLOWED, "Method not allowed"),
    };
    Box::new(future::ok(response))
}

pub fn try_file(path: &String, extensions: &Vec<String>, index: usize) -> PathBuf {
    if extensions.len() == index {
        return PathBuf::from("");
    }
    
    let extension = &extensions[index];
    let path = match extension.len() {
        0 => format!("{}", path),
        _ => format!("{}.{}", path, extension),
    };
    
    match File::open(path.clone()) {
        Ok(_) => PathBuf::from(path),
        Err(_) => try_file(&path, &extensions, index + 1),
    }
}

fn api_get_response(_req: &Request<Body>, uri: String, web_root: String, db: &Database) -> Response<Body> {
    match &*uri {
        // GET - /api/v1/question/unanswered - Get all unanswered questions
        "/api/v1/question/unanswered" => request_unanswered_question(db),
        _ => {
            if let Some(matched) = BOXY_API_V1_GET_URI.captures(&uri) {
                match matched.get(1) {
                    // GET - /api/v1/question/1 - Get question where `id=1`
                    Some(id) => request_question(&String::from(id.as_str()), db),
                    _ => response_with_error(400, StatusCode::BAD_REQUEST, "Missing question id"),
                }
            } else if let Some(matched) = BOXY_API_V1_GET_FROM_URI.captures(&uri) {
                match matched.get(1) {
                    // GET - /api/v1/question/from/1 - Get 30 questions from question where `id=1`
                    Some(id) => request_question_from(&String::from(id.as_str()), db),
                    _ => response_with_error(400, StatusCode::BAD_REQUEST, "Missing question id"),
                }
            } else {
                let mut normalized_path = normalize_path(&PathBuf::from(format!("{}{}", web_root, uri)));
                return if normalized_path.starts_with(web_root) {
                    if normalized_path.is_dir() {
                        normalized_path = normalized_path.join("index");
                    }
                    let html_extensions: Vec<String> = vec![String::from(""), String::from("html"), String::from("htm")];
                    let path_string = format!("{}", normalized_path.display());
                    let filepath = try_file(&path_string, &html_extensions, 0);
                    
                    match File::open(filepath.clone()) {
                        Ok(mut f) => {
                            let mut data = Vec::new();
                            match f.read_to_end(&mut data) {
                                Ok(_) => (),
                                Err(_) => return response_with_error(403, StatusCode::FORBIDDEN, "Access denied"),
                            };
                            let mime = match filepath.extension() {
                                Some(ext) => extension_to_mime(ext.to_str().unwrap()),
                                _ => "binary/octet-stream",
                            };
                            Response::builder()
                                .status(StatusCode::OK)
                                .header(CONTENT_TYPE, mime)
                                .body(Body::from(data)).unwrap()
                        },
                        Err(_) => response_with_error(404, StatusCode::NOT_FOUND, "Not found")
                    }
                } else {
                    response_with_error(403, StatusCode::FORBIDDEN, "Access denied")
                }
            }
        }
    }
}

fn api_post_response(req: Request<Body>, uri: String, remote_addr: SocketAddr, db: &Database) -> ResponseFuture {
    let db_ref = db.clone();
    let ip = get_real_ip(&req, &remote_addr.ip());
    
    Box::new(req.into_body().concat2().from_err().and_then(move |entire_body| {
        let body = String::from_utf8(entire_body.to_vec()).unwrap();
        let req_body: serde_json::Value = match serde_json::from_str(&body) {
            Ok(req) => req,
            Err(e) => return Ok(response_with_error(400, StatusCode::BAD_REQUEST, &format!("Invalid JSON string: {}", e))),
        };
        let verified = is_valid_token_from_json(&req_body, &db_ref);
        
        if !verified {
            // limit access rate for visitors
            // 5 posts (i.e., question creation) per hour (3600 seconds) per IP
            let access_rate_upper_bound = 5;
            let access_rate_statictics_duration = 3600;
            
            println!("[INFO] post request from {}", ip.to_string());
            
            let mut access_rate = ACCESS_RATE.lock().unwrap();
            let current_time = get_timestamp();
            if current_time - LAST_RESET_TIME.load(Ordering::SeqCst) > access_rate_statictics_duration {
                access_rate.clear();
                LAST_RESET_TIME.store(current_time, Ordering::Relaxed);
            } else {
                if let Some(access_rate) = access_rate.get_mut(&ip) {
                    *access_rate += 1;
                    if *access_rate > access_rate_upper_bound {
                        return Ok(response_with_error(429, StatusCode::TOO_MANY_REQUESTS, "Too many requests"));
                    }
                } else {
                    access_rate.insert(ip, 1);
                }
            }
        }
        
        return match &*uri {
            // POST - /api/v1/token - Token management
            // body:
            // {
            //    "token": "token"
            // }
            "/api/v1/token" => Ok(request_token(&req_body, &db_ref)),
            _ => {
                if let Some(matched) = BOXY_API_V1_QUESTION_URI.captures(&uri) {
                    match (matched.get(1), matched.get(2), matched.get(3), verified) {
                        // POST - /api/v1/answer/1 - Answer to question where `id=1`
                        // body:
                        // {
                        //    "answer": "Answer to the question."
                        // }
                        (None, _, Some(id), true) => Ok(request_answer_question(&req_body, &String::from(id.as_str()), &db_ref)),
                        // not verified
                        (None, _, Some(_), false) => Ok(response_with_error(403, StatusCode::FORBIDDEN, "Access denied")),
                        // POST - /api/v1/question - Create a question
                        // body:
                        // {
                        //    "question": "What's the question?",
                        // }
                        (Some(_), None, None, _) => Ok(request_create_question(&req_body, &db_ref)),
                        // impossible
                        (_, _, _, _) => Ok(response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, "It's impossible")),
                    }
                } else {
                    Ok(response_with_error(400, StatusCode::BAD_REQUEST, "No such API exists"))
                }
            }
        }
    }))
}

fn api_delete_response(req: Request<Body>, uri: String, db: &Database) -> ResponseFuture {
    let db_ref = db.clone();
    Box::new(req.into_body().concat2().from_err().and_then(move |entire_body| {
        let body = String::from_utf8(entire_body.to_vec()).unwrap();
        let req_body: serde_json::Value = match serde_json::from_str(&body) {
            Ok(req) => req,
            Err(e) => return Ok(response_with_error(400, StatusCode::BAD_REQUEST, &format!("Invalid JSON string: {}", e))),
        };
        let verified = is_valid_token_from_json(&req_body, &db_ref);
        
        if !verified {
            Ok(response_with_error(403, StatusCode::FORBIDDEN, "Access denied"))
        } else {
            if let Some(matched) = BOXY_API_V1_DELETE_URI.captures(&uri) {
                match (matched.get(1), matched.get(2)) {
                    (Some(delete_type), Some(id)) => match delete_type.as_str() {
                        // POST - /api/v1/question/1 - Delete a question where `id=1`
                        "question" => Ok(request_delete_question(&String::from(id.as_str()), &db_ref)),
                        // POST - /api/v1/answer/1 - Delete answer to question where `id=1`
                        "answer" => Ok(request_delete_answer(&String::from(id.as_str()), &db_ref)),
                        _ => Ok(response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, "It's impossible")),
                    },
                    (_, _) => Ok(response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, "It's impossible")),
                }
            } else {
                Ok(response_with_error(400, StatusCode::BAD_REQUEST, "No such API exists"))
            }
        }
    }))
}
