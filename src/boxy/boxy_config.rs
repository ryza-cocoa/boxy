use serde::Deserialize;
use std::option::Option;
use std::string::String;

#[derive(Deserialize, Debug, Clone)]
pub struct BoxyConfig {
    pub boxy_username: String,
    pub boxy_password: String,
    pub web_root: Option<String>,
    pub bind: Option<String>,
    pub port: Option<u16>,
    pub mongodb_url: String,
    pub mongodb_db: Option<String>,
    pub mongodb_username: Option<String>,
    pub mongodb_password: Option<String>,
}
