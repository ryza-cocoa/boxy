#[macro_use] macro_rules! read_json_str_field {
    ($var_name:ident, $from:ident, $field_name:ident) => {
        let $var_name = match $from[stringify!($field_name)].is_string() {
            true => String::from($from[stringify!($field_name)].as_str().unwrap()),
            false => return response_with_error(400, StatusCode::BAD_REQUEST, &format!("Malformed JSON string, missing `{}` field", stringify!($field_name))),
        };   
    }
}

#[macro_use] macro_rules! read_json_i64_field {
    ($var_name:ident, $from:ident, $field_name:ident) => {
        let $var_name = match $from[stringify!($field_name)].is_i64() {
            true => $from[stringify!($field_name)].as_i64().unwrap(),
            false => return response_with_error(400, StatusCode::BAD_REQUEST, &format!("Malformed JSON string, missing `{}` field", stringify!($field_name))),
        };   
    }
}

#[macro_use] macro_rules! from_bson_doc_field_to_json {
    ($bson_doc:ident, $field:ident, $json:ident, $type:ident) => {
        if let Some($field) = $bson_doc.get(stringify!($field)).and_then(Bson::$type) {
            $json.insert(stringify!($field).to_string(), serde_json::Value::from($field));
        } else {
            return None
        }
    } 
}

#[macro_use] macro_rules! from_bson_doc_to_json {
    ($bson_doc:ident, $json:ident) => {
        from_bson_doc_field_to_json!($bson_doc, id, $json, as_i64);
        from_bson_doc_field_to_json!($bson_doc, question_time, $json, as_i64);
        from_bson_doc_field_to_json!($bson_doc, question, $json, as_str);
        from_bson_doc_field_to_json!($bson_doc, answer, $json, as_str);
        from_bson_doc_field_to_json!($bson_doc, answer_time, $json, as_i64);
    } 
}