use bson::Bson;
use hyper::{Body, Response, StatusCode};
use mongodb::Database;
use super::boxy_helper::*;

lazy_static! {
    static ref TOKEN_SALT: String = String::from("token$a1t");
}

pub fn request_token(req_body: &serde_json::Value, db: &Database) -> Response<Body> {
    read_json_str_field!(req_type, req_body, type);
    
    match &*req_type {
        // generate new token
        "new" => request_new_token(&req_body, &db),
        // revoke token
        "revoke" => request_revoke_token(&req_body, &db),
        // revoke all token
        "revoke_all" => request_revoke_all_token(&req_body, &db),
        // No such API exists
        _ => response_with_error(400, StatusCode::BAD_REQUEST, "No such API exists"),
    }
}

pub fn is_valid_token_from_json(token: &serde_json::Value, db: &Database) -> bool {
    let token = match token["token"].is_string() {
        true => String::from(token["token"].as_str().unwrap()),
        false => return false,
    };
    is_valid_token(&token, db)
}

pub fn is_valid_token(token: &String, db: &Database) -> bool {
    let token_collection = db.collection("token");
    match token_collection.find_one(doc!{"token":token}, None).unwrap() {
        Some(token_doc) => {
            if let Some(expire_at) = token_doc.get("expire_at").and_then(Bson::as_i64) {
                if (get_timestamp() as i64) < expire_at {
                    return true
                }
            }
            false
        },
        _ => false
    }
}

fn request_new_token(data: &serde_json::Value, db: &Database) -> Response<Body> {
    read_json_str_field!(username, data, username);
    read_json_str_field!(password, data, password);
    read_json_i64_field!(time, data, time);
    
    let user_collection = db.collection("user");
    let user = match user_collection.find_one(doc!{"user": &username}, None).unwrap() {
        Some(user) => user,
        _ => return response_with_error(403, StatusCode::FORBIDDEN, &format!("Invalid username or password")),
    };
    let stored_pass = match user.get("password").and_then(Bson::as_str) {
        Some(pass) => pass,
        _ => return response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, &format!("That's cool, you don't have a password in the database")),
    };
    
    let token_time = get_timestamp() as i64;
    if token_time - time > 3 {
        return response_with_error(408, StatusCode::REQUEST_TIMEOUT, &format!("It takes too long, playback attack?"));
    }
    
    return if password == generate_hmac(&format!("{}{}", stored_pass, time), &TOKEN_SALT) {
        // expires after 365 days
        let token_expire_at: i64 = token_time + 365 * 24 * 60;
        let token = generate_hmac(&format!("{}{}", stored_pass, token_time), &TOKEN_SALT);
        let token_collection = db.collection("token");
        match token_collection.insert_one(doc!{"user":username, "token": &token, "token_time": token_time.clone(), "expire_at": token_expire_at.clone()}, None) {
            Ok(_) => {
                let mut resp = serde_json::map::Map::new();
                resp.insert("status".to_string(), serde_json::Value::from(0));
                resp.insert("token".to_string(), serde_json::Value::from(token));
                resp.insert("expire_at".to_string(), serde_json::Value::from(token_expire_at));
                stringify_result(&resp.into())
            },
            Err(e) => response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, &format!("Cannot insert into database: {}", e)),
        }
    } else {
        response_with_error(409, StatusCode::FORBIDDEN, &format!("Invalid username or password"))
    }
}

fn request_revoke_token(data: &serde_json::Value, db: &Database) -> Response<Body> {
    read_json_str_field!(token, data, token);
    match is_valid_token(&token, db) {
        true => {
            let token_collection = db.collection("token");
            match token_collection.delete_one(doc!{"token":token}, None) {
                Ok(_) => response_with_success(),
                Err(e) => response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, &format!("Cannot revoke token: {}", e)),
            }
        },
        false => response_with_error(409, StatusCode::FORBIDDEN, "Invalid token"),
    }
}

fn request_revoke_all_token(data: &serde_json::Value, db: &Database) -> Response<Body> {
    read_json_str_field!(token, data, token);
    match is_valid_token(&token, db) {
        true => {
            let token_collection = db.collection("token");
            match token_collection.drop(None) {
                Ok(_) => response_with_success(),
                Err(e) => response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, &format!("Cannot revoke all token: {}", e)),
            }
        },
        false => response_with_error(409, StatusCode::FORBIDDEN, "Invalid token"),
    }
}
