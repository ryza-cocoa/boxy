use hyper::{Body, Response, StatusCode};
use mongodb::Database;
use super::boxy_helper::{get_timestamp, response_with_error, response_with_success};

pub fn request_answer_question(req_body: &serde_json::Value, id: &String, db: &Database) -> Response<Body> {
    let id = match id.parse::<u64>() {
        Ok(id) => id,
        Err(e) => return response_with_error(400, StatusCode::BAD_REQUEST, &format!("Cannot answer the question requested because the type of `id` should be `u64`: {}", e)),
    };
    read_json_str_field!(answer, req_body, answer);
    
    let update_answer = doc! {
        "$set": {
            "answer": answer,
            "answer_time": get_timestamp()
        },
    };
    let question_collection = db.collection("question");
    let filter = doc! { "id": id };
    
    match question_collection.update_one(filter, update_answer, None).unwrap().modified_count {
        1 => response_with_success(),
        _ => response_with_error(404, StatusCode::NOT_FOUND, &format!("No answer were updated because no corresponding question with id={} exists", id)),
    }
}

pub fn request_delete_answer(id: &String, db: &Database) -> Response<Body> {
    let question_collection = db.collection("question");
    match question_collection.update_one(doc!{"id": id}, doc!{"$set": {"answer":"", "answer_time":0}}, None).unwrap().modified_count {
        1 => response_with_success(),
        _ => response_with_error(404, StatusCode::NOT_FOUND, &format!("No answer were deleted because no corresponding question id={} exists", id)),
    }
}
