use hmac::{Hmac, Mac};
use hyper::{Body, Request, Response, StatusCode, header};
use serde_json;
use sha2::Sha512;
use std::path::{Component, PathBuf};
use std::net::IpAddr;
use std::time::{SystemTime, UNIX_EPOCH};

// alias for HMAC-SHA512
type HmacSha512 = Hmac<Sha512>;

/// Generate HTTP 200 OK response
///
/// # Examples
///
/// ```
/// println!("{:?}", response_with_success());
/// ```
pub fn response_with_success() -> Response<Body> {
    Response::builder()
        .status(StatusCode::OK)
        .header(header::CONTENT_TYPE, "application/json")
        .body(Body::from("{\n  \"status\": 0\n}")).unwrap()
}

/// Generate failed response with HTTP status code and reason
///
/// # Examples
///
/// ```
/// println!("{:?}", response_with_error(400, StatusCode::BAD_REQUEST, "parameter missing"));
/// ```
pub fn response_with_error(error: i32, http_status_code: StatusCode, reason: &str) -> Response<Body> {
    let result = error_with_status_and_reason(error, reason);
    Response::builder()
        .status(http_status_code)
        .header(header::CONTENT_TYPE, "application/json")
        .body(Body::from(serde_json::to_string_pretty(&result).unwrap())).unwrap()
}

/// Generate the JSON result
///
/// # Examples
///
/// ```
/// error_with_status_and_reason(-1, "some error message")
/// ```
pub fn error_with_status_and_reason(status: i32, reason: &str) -> serde_json::Value {
    // init JSON dict for reporting error
    let mut error : serde_json::Value = serde_json::from_str("{}").unwrap();
    // set the status
    error["status"] = serde_json::Value::from(status);
    // set the reason
    error["reason"] = serde_json::Value::from(reason);
    
    error
}

/// Stringify the JSON result and put into box
///
/// # Examples
///
/// ```
/// let data : serde_json::Value = serde_json::from_str("{}").unwrap();
/// stringify_result(&data)
/// ```
pub fn stringify_result(result: &serde_json::Value) -> Response<Body> {
    // stringify the JSON dict
    let json = serde_json::to_string_pretty(result).unwrap();
    // return the result
    let body = Body::from(json);
    if let Some(http_status_code) = result["status"].as_i64() {
        match http_status_code {
            0 => {
                return Response::builder()
                    .status(StatusCode::OK)
                    .header(header::CONTENT_TYPE, "application/json")
                    .body(body).unwrap()
            },
            _ => match StatusCode::from_u16(http_status_code as u16) {
                Ok(http_status_code) => {
                    return Response::builder()
                        .status(http_status_code)
                        .header(header::CONTENT_TYPE, "application/json")
                        .body(body).unwrap();
                },
                _ => {
                    return Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        .header(header::CONTENT_TYPE, "application/json")
                        .body(body).unwrap();
                }
            }
        }
    }
    
    Response::builder()
        .status(StatusCode::INTERNAL_SERVER_ERROR)
        .header(header::CONTENT_TYPE, "application/json")
        .body(body).unwrap()
}

/// Generate HMAC-SHA512 of given message, secret and salt
///
/// # Examples
///
/// ```
/// println!("{}", generate_hmac("message", "salt"));
/// ```
pub fn generate_hmac(message: &str, salt: &str) -> String {
    // Create HMAC-SHA512 instance which implements `Mac` trait
    let mut mac = HmacSha512::new_varkey(format!("{}", salt).as_bytes())
        .expect("HMAC can take key of any size");
    mac.input(message.as_bytes());

    // `result` has type `MacResult` which is a thin wrapper around array of
    // bytes for providing constant time equality check
    let mut stringfied_hmac = String::new();
    for code in mac.result().code() {
        stringfied_hmac.push_str(&format!("{:02x}", code));
    }
    
    stringfied_hmac
}

pub fn get_timestamp() -> u64 {
    SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_secs()
}

/// Escape HTML
/// https://github.com/rust-lang/rust/blob/master/src/librustdoc/html/escape.rs
pub fn escape_html_entities(user_input: &String) -> String {
    let mut escaped: String = String::new();
    let pile_o_bits = user_input;
    let mut last = 0;
    for (i, ch) in user_input.bytes().enumerate() {
        match ch as char {
            '<' | '>' | '&' | '\'' | '"' => {
                escaped.push_str(&pile_o_bits[last..i]);
                let s = match ch as char {
                    '>' => "&gt;",
                    '<' => "&lt;",
                    '&' => "&amp;",
                    '\'' => "&#39;",
                    '"' => "&quot;",
                    _ => unreachable!(),
                };
                escaped.push_str(s);
                last = i + 1;
            }
            _ => {}
        }
    }
    if last < user_input.len() {
        escaped.push_str(&pile_o_bits[last..]);
    }
    escaped
}

#[macro_use] macro_rules! get_ip_in_header {
    ($header:ident, $field:literal) => {
        if let Some(ip_in_header) = $header.get($field) {
            match ip_in_header.to_str() {
                Ok(ip_in_header) => {
                    if let Ok::<IpAddr, _>(client_ip) = String::from(ip_in_header).split(',').map(|splited| splited.trim()).collect::<Vec<&str>>()[0].parse::<IpAddr>() {
                        return client_ip;
                    }
                },
                Err(_) => (),
            }
        }
    }
}

pub fn get_real_ip(req: &Request<Body>, remote_addr: &IpAddr) -> IpAddr {
    let headers = req.headers();

    get_ip_in_header!(headers, "cf-connecting-ip");
    get_ip_in_header!(headers, "x-forwarded-for");
    get_ip_in_header!(headers, "x-real-ip");
    get_ip_in_header!(headers, "client-ip");
    
    remote_addr.clone()
}

// https://github.com/rust-lang/rust/pull/47363
pub fn normalize_path(p: &PathBuf) -> PathBuf {
    let mut stack: Vec<Component> = vec![];

    // We assume .components() removes redundant consecutive path separators.
    // Note that .components() also does some normalization of '.' on its own anyways.
    // This '.' normalization happens to be compatible with the approach below.
    for component in p.components() {
        match component {
            // Drop CurDir components, do not even push onto the stack.
            Component::CurDir => {},

            // For ParentDir components, we need to use the contents of the stack.
            Component::ParentDir => {
                // Look at the top element of stack, if any.
                let top = stack.last().cloned();

                match top {
                    // A component is on the stack, need more pattern matching.
                    Some(c) => {
                        match c {
                            // Push the ParentDir on the stack.
                            Component::Prefix(_) => { stack.push(component); },

                            // The parent of a RootDir is itself, so drop the ParentDir (no-op).
                            Component::RootDir => {},

                            // A CurDir should never be found on the stack,
                            // since they are dropped when seen.
                            Component::CurDir => { unreachable!(); },

                            // If a ParentDir is found, it must be due to it
                            // piling up at the start of a path.
                            // Push the new ParentDir onto the stack.
                            Component::ParentDir => { stack.push(component); },

                            // If a Normal is found, pop it off.
                            Component::Normal(_) => { let _ = stack.pop(); }
                        }
                    },

                    // Stack is empty, so path is empty, just push.
                    None => { stack.push(component); }
                }
            },

            // All others, simply push onto the stack.
            _ => { stack.push(component); },
        }
    }

    // If an empty PathBuf would be returned, instead return CurDir ('.').
    if stack.is_empty() {
        return PathBuf::from(Component::CurDir.as_os_str());
    }

    let mut norm_path = PathBuf::new();

    for item in &stack {
        norm_path.push(item.as_os_str());
    }

    norm_path
}

pub fn extension_to_mime(ext: &str) -> &'static str {
    match ext {
        "html" | "htm" => "text/html",
        "js" => "text/javascript",
        "css" => "text/css",
        "jpeg" | "jpg" => "image/jpeg",
        "webp" => "image/webm",
        "webm" => "video/webm",
        "png" => "image/png",
        "gif" => "image/gif",
        "ttf" => "font/ttf",
        "woff" => "font/woff",
        "woff2" => "font/woff2",
        "svg" => "text/xml",
        "mp3" => "audio/mpeg",
        "pdf" => "application/pdf",
        "weba" => "audio/webm",
        _ => "binary/octet-stream",
    }
}
