use bson::{Bson, ordered::OrderedDocument};
use hyper::{Body, Response, StatusCode};
use mongodb::{Database, options::FindOptions, options::FindOneAndUpdateOptions};
use super::boxy_helper::*;

pub fn request_create_question(req_body: &serde_json::Value, db: &Database) -> Response<Body> {
    read_json_str_field!(question, req_body, question);
    create_question(&question, db)
}

pub fn request_question(id: &String, db: &Database) -> Response<Body> {
    let id = match id.as_str().parse::<i64>() {
        Ok(id) => id,
        Err(e) => return response_with_error(400, StatusCode::BAD_REQUEST, &format!("`id` should be a positive number and should be i64: {}", e)),
    };

    let question_collection = db.collection("question");
    let document = match question_collection.find_one(doc!{"id": id}, None).unwrap() {
        Some(document) => match question_doc_to_json(&document) {
            Some(document) => document,
            _ => return response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, &format!("Cannot convert question id={} to JSON", id)),
        },
        _ => return response_with_error(404, StatusCode::NOT_FOUND, &format!("No question with id={} exists", id)),
    };

    let mut result = serde_json::map::Map::new();
    result.insert("status".to_string(), serde_json::Value::from(0));
    result.insert("question".to_string(), serde_json::Value::from(document));
    stringify_result(&result.into())
}

pub fn request_question_from(id: &String, db: &Database) -> Response<Body> {
    let id = match id.as_str().parse::<i64>() {
        Ok(id) => id,
        Err(e) => return response_with_error(400, StatusCode::BAD_REQUEST, &format!("`id` should be a positive number and should be i64: {}", e)),
    };
    
    let question_collection = db.collection("question");
    let find_options = FindOptions::builder().sort(doc! { "id": -1 }).limit(30).build();
    let cursor = match question_collection.find(doc!{"id": {"$lt": id}}, find_options) {
        Ok(cursor) => cursor,
        _ => return response_with_error(404, StatusCode::NOT_FOUND, &format!("No question with id={} exists", id)),
    };

    let mut result = serde_json::map::Map::new();
    let mut questions: Vec<serde_json::Value> = Vec::new();
    for result in cursor {
        match result {
            Ok(document) => match question_doc_to_json(&document) {
                Some(document) => questions.push(document),
                _ => (),
            },
            Err(_) => (),
        }
    }
    result.insert("status".to_string(), serde_json::Value::from(0));
    result.insert("questions".to_string(), serde_json::Value::from(questions));
    stringify_result(&result.into())
}

pub fn request_delete_question(id: &String, db: &Database) -> Response<Body> {
    let id = match id.as_str().parse::<i64>() {
        Ok(id) => id,
        Err(e) => return response_with_error(400, StatusCode::BAD_REQUEST, &format!("`id` should be a positive number and should be i64: {}", e)),
    };
    
    let question_collection = db.collection("question");
    match question_collection.delete_one(doc!{"id": id}, None).unwrap().deleted_count {
        1 => response_with_success(),
        _ => response_with_error(404, StatusCode::NOT_FOUND, &format!("No question were deleted because question with id={} does not exist", id)),
    }
}

pub fn request_unanswered_question(db: &Database) -> Response<Body> {
    let question_collection = db.collection("question");
    let mut unanswered_id: Vec<String> = Vec::new();
    let cursor = question_collection.find(doc!{"answer_time": 0}, None).unwrap();
    
    // Iterate over the results of the cursor.
    for result in cursor {
        match result {
            Ok(document) => {
                if let Some(id) = document.get("id").and_then(Bson::as_str) {
                    unanswered_id.push(id.to_string());
                }
            }
            Err(e) => eprintln!("[ERROR] Error occurred while finding all unanswered questions: {}", e),
        }
    }
    
    let mut unanswered: serde_json::Value = serde_json::Value::from(unanswered_id);
    unanswered["status"] = serde_json::Value::from(0);
    stringify_result(&unanswered)
}

fn question_doc_to_json(question: &OrderedDocument) -> Option<serde_json::Value> {
    let mut result = serde_json::map::Map::new();
    from_bson_doc_to_json!(question, result);
    Some(result.into())
}

fn create_question(question: &String, db: &Database) -> Response<Body> {
    let question_seq_collection = db.collection("question_seq");
    let seq: i64 = question_seq_collection.estimated_document_count(None).unwrap();
    if seq == 0 {
        match question_seq_collection.insert_one(doc!{"id":"question", "seq":1i64}, None) {
            Ok(_) => (),
            Err(e) => return response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, &format!("Cannot initialize question seq id: {}", e)),
        }
    }
    let options = FindOneAndUpdateOptions::builder().upsert(true).build();
    let id: i64 = match question_seq_collection.find_one_and_update(doc!{"id":"question"}, doc!{"$inc": {"seq":1}}, options) {
        Ok(document) => {
            if let Some(id) = document.unwrap().get("seq").and_then(Bson::as_i64) {
                id
            } else {
                return response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, &format!("No question seq found from database"));
            }
        },
        Err(e) => return response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, &format!("Cannot update question seq id from database: {}", e)),
    };
    
    let question_collection = db.collection("question");
    // question = {
    //    "question_time": 1,
    //    "question": "what's the question",
    //    "answer": "",
    //    "answer_time": 1,
    //    "id": 1
    // }
    
    println!("[INFO] question: {}", question);
    match question_collection.insert_one(doc! {
        "question": escape_html_entities(&question),
        "question_time": get_timestamp(),
        "answer": "",
        "answer_time": 0i64,
        "id": id
    }, None) {
        Ok(_) => {
            let mut resp: serde_json::Value = serde_json::from_str(r#"{"status":0}"#).unwrap();
            resp["id"] = serde_json::Value::from(id);
            stringify_result(&resp)
        },
        Err(e) => response_with_error(500, StatusCode::INTERNAL_SERVER_ERROR, &format!("Cannot insert question into database: {}", e)),
    }
}
