$(document).ready(function(){
    var token = localStorage.getItem('token');
    var expire_at = localStorage.getItem('expire_at');
    if (token != null && token.length != 0 && expire_at != null && expire_at > Math.floor(Date.now() / 1000)) {
        document.location = "/";
    }
    
    $("#login").click(() => {
        var password_salt = "boxy$a1t";
        var token_salt = "token$a1t";
        var username = $("#username").val();
        var password = $("#password").val();
        if (username.length == 0 || password.length == 0) {
            alert("Please input username and password");
        } else {
            function hmacsha512(message, key, cb) {
                window.crypto.subtle.importKey("raw", new TextEncoder("utf-8").encode(key), {"name": "HMAC", "hash":"SHA-512"}, false, ["sign"]).then(signkey => {
                    window.crypto.subtle.sign("HMAC", signkey, new TextEncoder("utf-8").encode(message)).then(hmac => {
                        cb(Array.prototype.map.call(new Uint8Array(hmac), x=>(('00'+x.toString(16)).slice(-2))).join(''));
                    });
                });
            }
            
            hmacsha512(password + password_salt, password_salt, (hmac) => {
                var current_time = Math.floor(Date.now() / 1000);
                hmacsha512(hmac + current_time, token_salt, (token_hmac) => {
                    var post_data = JSON.stringify({
                        "username":username,
                        "password":token_hmac,
                        "time": current_time,
                        "type": "new",
                    });
                    $.ajax({
                        type: "POST",
                        url: "/api/v1/token",
                        data: post_data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: result => {
                            if (result["status"] == 0) {
                                localStorage.setItem('token', result["token"]);
                                localStorage.setItem('expire_at', result["expire_at"]);
                                document.location = "/";
                            } else {
                                console.log(result["reason"]);
                            }
                        },
                        failure: console.log,
                    });
                });
            });
        }
    });
});
