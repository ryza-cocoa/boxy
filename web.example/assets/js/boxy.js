var questions = {};
var min_id = 0x7fffffff;
var max_id = -1;
var no_more_question_below_id = min_id;

var token = localStorage.getItem("token");
var expire_at = localStorage.getItem("expire_at");
var enabled_edit = false;
$(".signin-out").click(() => {
    document.location = "/login";
});

function remove_token_and_signin() {
    localStorage.removeItem("token");
    localStorage.removeItem("expire_at");
    document.location = "/login";
}

if (token != null && token.length != 0 && expire_at != null && expire_at > Math.floor(Date.now() / 1000)) {
    enabled_edit = true;
    $(".signin-out").html("Sign Out");
    $(".signin-out").click(() => {
        remove_token_and_signin();
    });
}

function display_questions() {
    var keys = Object.keys(window.questions).sort((a, b) => {return (a["id"] - b["id"]) > 0});
    window.max_id = keys[0];
    window.min_id = keys[keys.length - 1];
    $(".boxy").html("");
    for (var index = 0; index < keys.length; index++) {
        var key = keys[index];
        var question = window.questions[key];
        if (window.enabled_edit) {
            let question_id = "boxy-q-" + key;
            let remove_btn_id = "remove-btn-" + key;
            $(".boxy").append("<div><p class='boxy-q' id='" + question_id + "'>Q["+key+"]: "+question["question"]+"<button class='answer-input-btn remove-btn' id='" + remove_btn_id + "' data-action='remove'><i class='fa fa-close'></i></button></p><p class='boxy-a' id='answer-"+question["id"]+"'data-question-id='"+question["id"]+"'>A: "+question["answer"]+"</p></div>");
            
            $("#"+remove_btn_id).css('opacity', '0');
            $("#"+question_id).hover(() => {
                $("#"+remove_btn_id).css('opacity', '1');
            }, () => {
                $("#"+remove_btn_id).css('opacity', '0');
            });
        } else {
            $(".boxy").append("<div><p class='boxy-q'>Q["+key+"]: "+question["question"]+"</p><p class='boxy-a' id='answer-"+question["id"]+"'data-question-id='"+question["id"]+"'>A: "+question["answer"]+"</p></div>");
        }
    }
    if (window.no_more_question_below_id != window.min_id) {
        $(".boxy").append("<div class='btn btn-boxy btn-loadmore'><span>load more</span></div>");
        $(".btn-loadmore").click(() => {
            show_questions_from(window.min_id);
        });
    }
    if (window.enabled_edit) {
        $(".remove-btn").click(event => {
            var current_target = event.currentTarget;
            if (current_target.attributes["data-action"] == null) {return;}
            var action = current_target.attributes["data-action"].value;
            if (action == 'remove') {
                var id = current_target.parentNode.attributes["id"].value.substr("boxy-q-".length);
                console.log("remove ", id);
                $.ajax({
                    type: "DELETE",
                    url: "/api/v1/question/" + id,
                    data: JSON.stringify({"token":token}),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: result => {
                        if (result["status"] != 0) {
                            if (result["status"] == 403) {
                                alert("Token expired or removed, please sign in again.");
                                remove_token_and_signin();
                            } else {
                                alert(result["reason"]);
                            }
                        } else {
                            delete window.questions[id];
                            $("#boxy-q-"+id).remove();
                            $("#answer-"+id).remove();
                        }
                    },
                    failure: console.log,
                });
            }
            event.stopPropagation();
        });
        
        $(".boxy-a").click(on => {
            if (on.target.childNodes.length != 1){return;}
            if (expire_at < Math.floor(Date.now() / 1000)) { document.location = "/login"; }
            
            var question_id = on.target.attributes["data-question-id"].value;
            var answer = window.questions[question_id]["answer"];
            $("#answer-"+question_id).html("A: <input class='answer-input answer-input-" + question_id + "' type='text' value='"+answer+"'></input><button class='answer-input-btn' data-action='confirm'><i class='fa fa-check'></i></button><button class='answer-input-btn' data-action='cancel'><i class='fa fa-close'></i></button>");
            $(".answer-input").click(event => {
                event.stopPropagation();
            });

            $(".answer-input-btn").click(event => {
                var current_target = event.currentTarget;
                if (current_target.attributes["data-action"] == null) {return;}
                
                var action = current_target.attributes["data-action"].value;
                if (action == 'cancel') {
                    var parent_node = current_target.parentNode;
                    var id = parent_node.attributes["data-question-id"].value;
                    $("#answer-"+id).html("A: " + window.questions[id]["answer"]);
                } else if (action == 'confirm') {
                    var id = current_target.parentNode.attributes["data-question-id"].value;
                    var new_answer = $(".answer-input-" + question_id).val();
                    $.ajax({
                        type: "POST",
                        url: "/api/v1/answer/" + id,
                        data: JSON.stringify({"answer":new_answer, "token":token}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: result => {
                            if (result["status"] != 0) {
                                if (result["status"] == 403) {
                                    alert("Token expired or removed, please sign in again.");
                                    remove_token_and_signin();
                                } else {
                                    alert(result["reason"]);
                                }
                            } else {
                                window.questions[id]["answer"] = new_answer;
                                $("#answer-"+id).html("A: " + window.questions[id]["answer"]);
                            }
                        },
                        failure: console.log,
                    });
                }
            });
        });
    }
}

function show_questions_from(id) {
    $.ajax({
        type: "GET",
        url: "/api/v1/question/from/" + id,
        success: result => {
            if (result["status"] != 0) {
                alert(result["reason"]);
            } else {
                var result_questions = result["questions"];
                if (result_questions.length > 0) {
                    for (var index = 0; index < result_questions.length; index++) {
                        var q = result_questions[index];
                        window.questions[q["id"]] = q;
                    }
                    display_questions();
                } else {
                    window.no_more_question_below_id = id;
                    if (window.no_more_question_below_id == window.min_id) {
                        $(".btn-loadmore").remove();
                    }
                    console.log("no questions");
                }
            }
        },
        failure: console.log,
    });
}

$(document).ready(function(){
    show_questions_from(window.min_id);
    $(".btn-send").click(() => {
        var question = $(".question").val();
        if (question.length == 0) {
            alert("Nothing to answer since nothing asked");
        } else {
            var post_data = window.enabled_edit ? JSON.stringify({"question":question, "token":window.token}) : JSON.stringify({"question":question});
            $.ajax({
                type: "POST",
                url: "/api/v1/question",
                data: post_data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: result => {
                    if (result["status"] == 0) {
                        $(".question").val("");
                        show_questions_from(result["id"] + 1);
                    } else {
                        if (result["status"] == 429) {
                            alert("Too many questions x_x");
                        }
                        console.log(result["reason"]);
                    }
                },
                failure: console.log,
            });
        }
    });
});
