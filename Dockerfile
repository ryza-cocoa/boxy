# Copyright (c) Ryza
# Distributed under the terms of the Modified BSD License.

FROM rust:1.40

LABEL maintainer="Ryza <1-ryza@users.noreply.magic.ryza.moe>"

COPY ./ /root/boxy
WORKDIR /root/boxy

RUN cargo build --release && \
    cp target/release/boxy /usr/local/bin/boxy && \
    rm -rf /root/boxy/target

CMD ["/usr/local/bin/boxy", "/etc/boxy/boxy.json"]
